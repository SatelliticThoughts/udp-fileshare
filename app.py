#!/usr/bin/env python3


from client import Client
from server import Server


import argparse


if __name__ == "__main__":
	IP = "127.0.0.1"
	PORT = 5522
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--file",
						help="File for client to send")
	parser.add_argument("-c", "--client", 
						help="Start Application as client",
						action="store_true")
	parser.add_argument("-s", "--server",
						help="Start Application as server",
						action="store_true")
	
	args = parser.parse_args()
	
	
	if args.client:
		f = args.file
		print("Client running on", IP, PORT)
		client = Client(IP, PORT, f)
		client.run()
	
	if args.server:
		print("Server running on", IP, PORT)
		server = Server(IP, PORT)
		server.run()
