#!/usr/bin/env python3


import socket


class Server:
	def __init__(self, ip, port):
		self._ip = ip
		self._port = port
		self._buffer_size = 1024
		
	def run(self):
		response = str.encode("Packets Recieved")
		udp = socket.socket(family=socket.AF_INET, 
									type=socket.SOCK_DGRAM)
		udp.bind((self._ip, self._port))
		count = 0
		while(True):
			bytes_address_pair = udp.recvfrom(self._buffer_size)

			message = bytes_address_pair[0]
			address = bytes_address_pair[1]

			client_msg = "Message from Client:{}".format(message)
			client_ip  = "Client IP Address:{}".format(address)
    
			print(count, client_ip, client_msg)
			count += 1
			open("test.wav", "br").write(open("test.wav", "br").read() + message)
			
			udp.sendto(response, address)
