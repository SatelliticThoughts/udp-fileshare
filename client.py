#!/usr/bin/env python3


import socket


class Client:
	def __init__(self, ip, port, filepath):
		self._ip = ip
		self._port = port
		self._file = open(filepath, "rb")
		self._buffer_size = 1024

	def run(self):
		udp = socket.socket(family=socket.AF_INET, 
							type=socket.SOCK_DGRAM)
 
		count = 0
		while True:
			data = self._file.read(self._buffer_size)
			if data == b'':
				break
			
			udp.sendto(data, (self._ip, self._port))

			response = udp.recvfrom(self._buffer_size)

			msg = "{}: Message from Server {}".format(count, response[0])
			count += 1
			print(msg)
			
		self._file.close()
